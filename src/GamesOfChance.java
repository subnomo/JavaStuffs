import java.util.Scanner;

public class GamesOfChance {

	/**
	 * Casino Simulator 2015
	 */

	// Creates the scanner for getting input
	private static Scanner readData = new Scanner(System.in);
	
	// Main method
	public static void main(String[] args) throws InterruptedException {
		new GamesOfChance();
	}
	
	// Pseudo-global (local to class) variable money to alter in any method
	static long money = 1000L;
	
	// Constructor
	public GamesOfChance() throws InterruptedException { // InterruptedException is
		boolean cont = true;							 // required for Thread.sleep()
		while (cont) {
			money = 1000L;
			cont = play();
		}
		System.out.println("\n\nGoodbye...");
	}

	// This method manages and loops through all of the step methods
	public static boolean play() throws InterruptedException {
		System.out.println("\n\nWhat step do you want to play?");
		System.out.println("1. Roulette\n2. Blackjack\n3. Slot Machine\n4. True Roulette");
		int game = readData.nextInt();
		System.out.println("Enter the amount you will wager (0 - " + money + "): ");
		int wager = readData.nextInt();
		
		if (wager < 0) {
			System.out.println("You can not wager our money.");
			return false;
		} else if (wager == 0) {
			System.out.println("You must wager some money.");
			return false;
		} else if (wager > money) {
			System.out.println("You don't have that much money to wager.");
			return false;
		}
		
		int rounds = 0;
		if (game < 4) {
			System.out.println("How many rounds would you like to play?");
			rounds = readData.nextInt();
		}
		
		String repeat = "n";
		switch (game) {
		case 1:
			games(47.4, wager, rounds);
			System.out.println("Play again? (y/n)");
			repeat = readData.next();
			if ("yes".equals(repeat) || "y".equals(repeat)) return true;
			else return false;
		case 2:
			games(49.2, wager, rounds);
			System.out.println("Play again? (y/n)");
			repeat = readData.next();
			if ("yes".equals(repeat) || "y".equals(repeat)) return true;
			else return false;
		case 3:
			games(46, wager, rounds);
			System.out.println("Play again? (y/n)");
			repeat = readData.next();
			if ("yes".equals(repeat) || "y".equals(repeat)) return true;
			else return false;
		case 4:
			trueRoulette(wager);
			if (money < 0) {
				System.out.println("You are $" + Math.abs(money) + " in debt. Play again? (y/n)");
			} else if (money == 0) {
				System.out.println("You have no money left. Play again? (y/n)");
			} else {
				System.out.println("You have $" + money + ". Play again? (y/n)");
			}
			repeat = readData.next();
			if ("yes".equals(repeat) || "y".equals(repeat)) return true;
			else return false;
		default:
			System.out.println("Error, unexpected input.");
		}
		return false;
	}
	
	public static void games(double chance, int wager, int rounds) {
		int wins = 0, losses = 0;
		for (int i = 0; i <= rounds; i++) {
			if (money <= 0) break;
			
			double randNum = (double) (Math.random() * (101));
			if (randNum <= chance) {
				money += wager;
				wins++;
			} else {
				money -= wager;
				losses++;
			}
		}
		
		System.out.println("\n\nWon: " + wins);
		System.out.println("Lost: " + losses);
		if (money > 0) System.out.println("Money remaining: $" + money);
		else System.out.println("Debt: $" + Math.abs(money));
	}
	
	// This simulates a wheel spin
	public static void wheel(int randNum) throws InterruptedException {
		for (int i = 0; i <= 80; i++) {
			if (i == 80) {
				System.out.println("\t\t" + randNum);
				Thread.sleep(500);
				break;
			} else if (i % 4 == 0) {
				int crabNum = (int) (Math.random() * (37));
				System.out.println("\t\t" + crabNum);
				Thread.sleep(100); // Sleeps to give wheel effect
			}
			System.out.println("\n\n\n\n\n");
		}
	}
	
	// This implements a full roulette step
	public static void trueRoulette(int wager) throws InterruptedException {
		int randNum = (int) (Math.random() * (37));
		System.out.println(randNum);
		System.out.println("\nEnter a number 0-36 or choose even or odd:");
		String choice = readData.next();
		if ("even".equals(choice)) {
			System.out.println("\n\n\n\n\n");
			wheel(randNum);

			if (randNum % 2 == 0) { //Win
				money += wager;
				System.out.println("You win $" + wager + ".");
			} else { //Lose
				money -= wager;
				System.out.println("You lose $" + wager + ".");
			}
		} else if ("odd".equals(choice)) {
			wheel(randNum);

			if (randNum % 2 != 0) {
				money += wager;
				System.out.println("You win $" + wager + ".");
			} else {
				money -= wager;
				System.out.println("You lose $" + wager + ".");
			}
		} else {
			int numChoice = Integer.parseInt(choice);
			wheel(randNum);

			if (randNum == numChoice) {
				money += wager * 35 + 1;
				System.out.println("You win $" + (wager * 35 + 1) +".");
			} else {
				money--;
				System.out.println("You lose $1.");
			}
		}
	}
}
