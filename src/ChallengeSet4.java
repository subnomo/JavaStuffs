import java.util.Scanner;

public class ChallengeSet4 {

	// Creates the scanner for getting input
	private static Scanner readData = new Scanner(System.in);
	
	public static void main(String[] args) {
		new ChallengeSet4();
	}

	public ChallengeSet4() {
		/*k70();
		multTable();
		System.out.println("Enter the amount of money in your bank account: ");
		double money = readData.nextDouble();
		System.out.println("Enter the percentage interest rate: ");
		double interestRate = readData.nextDouble();
		String[] years = calcInterest(money, interestRate, 30, 1, 0);
		for (int i = 0; i <= 30; i++) System.out.println(years[i]);*/
		//realisticInterest();
		picture();
	}

	public static void k70() {
		for (int i = 0; i <= 70000; i += 7)
			System.out.println(i);
	}
	
	public static void multTable() {
		System.out.println("Enter the first number to multiply: ");
		int h = readData.nextInt();
		System.out.println("Enter the second number: ");
		int w = readData.nextInt();
		for (int i = 1; i <= h; i++) {
			for (int j = 1; j <= w; j++) {
				System.out.print(i * j + "\t");
			}
			System.out.println();
		}
	}
	
	public static String[] calcInterest(double money, double interestRate, int numYears, int timesCompounded, double saved) {
		double interestFin = interestRate / 100;
		System.out.println(interestFin);
		
		String[] years;
		String time;
		if (timesCompounded == 1) {
			time = "Year ";
			years = new String[31]; // Why am I doing it this way
		} else {
			time = "Month ";
			years = new String[361];
		}
		
		for (int i = 0; i <= (numYears * timesCompounded); i++) {
			if (i == 0) {
				years[i] = time + i + ": " + money;
			} else {
				if (i % 12 == 0) {
					money += saved;
					System.out.println(money);
					years[i] = time + i + ": " + money * Math.pow(1.0 + (interestFin / timesCompounded), i);
				}
			}
		}
		
		return years;
	}
	
	public static void realisticInterest() {
		System.out.println("Enter the amount of money in your bank account: ");
		double money = readData.nextDouble();
		System.out.println("Enter the percentage interest rate: ");
		double interestRate = readData.nextDouble();
		System.out.println("Enter the amount you will save each month: ");
		double saved = readData.nextDouble();
		
		String[] years = calcInterest(money, interestRate, 30, 12, saved);
		for (int i = 0; i <= 360; i+=12) System.out.println(years[i]);
	}
	
	public static void picture() {
		// System.out.println("Enter the height of the diamond: ");
		// int height = readData.nextInt();
		int height = 10;
		if (height % 2 == 0) height -= 1; // Ensures height is odd
		boolean flip = false;
		int star = 1;
		for (int r = 0 - height/2; r <= height/2; r++) {
			System.out.println("r: " + r);
			if (r == 0 - height/2 || r == height/2); //System.out.println("\n*");
			else {
				int endval = Math.abs(r);
				if (star == 0) {
					endval = Math.abs(r) - 1;
				}
				/*for (int i = 1; i <= endval; i++) { // Space counter
					System.out.println("i: " + i);
					if (i == 1 && flip == false) {
						System.out.print("*");
						flip = true;
					}

					System.out.print(" ");
					if (i == 0) System.out.println("bro");
					//if (i == 0 && flip == true) System.out.print("aa");
					i = (Math.abs(r) - 1) - i;
					  System.out.println(i);
					  Goes like: 3210123 (spaces)
					  I want: 0123210
					if (i == height / 2 && flip == false) {
						flip = true;
						i = 1;
					}
				}
				System.out.print("*");
				++star;
				if (star == 2) {
					System.out.print("\n");
					star = 0;
				}*/
			}
		}
	}
	
}
