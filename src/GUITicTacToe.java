import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUITicTacToe implements ActionListener {

    // Sets up everything we need to get started
    JFrame frame = new JFrame("Tic Tac Toe - X's Turn");
    JButton[][] jButtons = new JButton[3][3];
    Container center = new Container();
    Container north = new Container();
    JLabel xLabel = new JLabel("X's wins: 0");
    JLabel oLabel = new JLabel("O's wins: 0");
    JButton xChangeName = new JButton("Change X's Name: ");
    JButton oChangeName = new JButton("Change O's Name: ");
    JTextField xChangeField = new JTextField();
    JTextField oChangeField = new JTextField();
    char[][] board = new char[3][3];
    char player = 'X';
    String xPlayerName = "X";
    String oPlayerName = "O";
    int xWins = 0, oWins = 0;

    public static void main(String args[]) {
        new GUITicTacToe();
    }

    public GUITicTacToe() {
        // Sets up the window
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());

        // The center layout
        center.setLayout(new GridLayout(3, 3));
        for (int r = 0; r < jButtons.length; r++) {
            for (int c = 0; c < jButtons[0].length; c++) {
                jButtons[c][r] = new JButton();
                center.add(jButtons[c][r]);
                jButtons[c][r].addActionListener(this);
            }
        }

        frame.add(center, BorderLayout.CENTER);

        // The north layout
        north.setLayout(new GridLayout(3, 2));
        north.add(xLabel);
        north.add(oLabel);
        frame.add(north, BorderLayout.NORTH);
        north.add(xChangeName);
        north.add(oChangeName);
        north.add(xChangeField);
        north.add(oChangeField);

        xChangeName.addActionListener(this);
        oChangeName.addActionListener(this);

        frame.setVisible(true);
    }

    @Override
    // Get click, change button accordingly
    public void actionPerformed(ActionEvent e) {
        JButton current;

        for (int r = 0; r < jButtons.length; r++) {
            for (int c = 0; c < jButtons[0].length; c++) {
                if (e.getSource().equals(jButtons[c][r])) {
                    current = jButtons[c][r];
                    // If spot empty
                    if (board[c][r] == 0) {
                        if (player == 'X') {
                            frame.setTitle("Tic Tac Toe - " + oPlayerName + "'s Turn");
                            current.setText("X");
                            board[c][r] = player;
                            player = 'O';
                            current.setEnabled(false);
                        } else {
                            frame.setTitle("Tic Tac Toe - " + xPlayerName + "'s Turn");
                            current.setText("O");
                            board[c][r] = player;
                            player = 'X';
                            current.setEnabled(false);
                        }
                    }

                    if (checkWin()) {
                        // Increments wins, sets label wins
                        if (player == 'X') {
                            ++oWins;
                            oLabel.setText(oPlayerName + "'s wins: " + oWins);
                            frame.setTitle("Tic Tac Toe - " + xPlayerName + "'s Turn");
                        } else {
                            ++xWins;
                            xLabel.setText(xPlayerName + "'s wins: " + xWins);
                            frame.setTitle("Tic Tac Toe - " + xPlayerName + "'s Turn");
                        }
                        clearBoard();

                    } else if (catsGame()) {
                        clearBoard();
                    }
                } else if (e.getSource().equals(xChangeName)) {
                    xPlayerName = xChangeField.getText();
                    if (xPlayerName.substring(xPlayerName.length() - xPlayerName.length()).equals("") || xPlayerName.substring(xPlayerName.length() - 1).equals(" "))
                        xPlayerName = "X";
                    xLabel.setText(xPlayerName + "'s wins: " + xWins);

                    if (player == 'X')
                        frame.setTitle("Tic Tac Toe - " + xPlayerName + "'s Turn");
                } else if (e.getSource().equals(oChangeName)) {
                    oPlayerName = oChangeField.getText();
                    oLabel.setText(oPlayerName + "'s wins: " + oWins);

                    if (player == 'O')
                        frame.setTitle("Tic Tac Toe - " + oPlayerName + "'s Turn");
                }
            }
        }
    }

    // Clears the board on win/tie
    public void clearBoard() {
        for (int r = 0; r < board.length; r++) {
            for (int c = 0; c < board[0].length; c++) {
                board[c][r] = 0;
                jButtons[c][r].setText("");
                jButtons[c][r].setEnabled(true);
            }
        }
        player = 'X';
    }

    // Checks board for winner
    public boolean checkWin() {
        // Checks rows for win
        for (int r = 0; r < 3; r++) {
            // Tracks how many X's and O's are in the row
            int rowX = 0, rowO = 0;
            for (int c = 0; c < 3; c++) {
                if (board[r][c] == 'X') ++rowX;
                else if (board[r][c] == 'O') ++rowO;
            }

            if (rowX == 3 || rowO == 3) return true;
        }

        // Checks columns for win
        for (int c = 0; c < 3; c++) {
            // Tracks how many X's and O's are in the column
            int colX = 0, colO = 0;
            for (int r = 0; r < 3; r++) {
                if (board[r][c] == 'X') ++colX;
                else if (board[r][c] == 'O') ++colO;
            }

            if (colX == 3 || colO == 3) return true;
        }

        // Checks diagonals for win
        int diagX = 0, diagO = 0;
        for (int d = 0; d < 3; d++) {
            if (board[d][d] == 'X') ++diagX;
            else if (board[d][d] == 'O') ++diagO;
        }

        if (diagX == 3 || diagO == 3) return true;

        diagX = 0;
        diagO = 0;
        int dec = 2;
        for (int inc = 0; inc < 3; inc++) {
            if (board[dec][inc] == 'X') ++diagX;
            else if (board[dec][inc] == 'O') ++diagO;
            --dec;
        }

        if (diagX == 3 || diagO == 3) return true;

        return false;
    }

    // Checks board for cat's game
    public boolean catsGame() {
        int rowFilled = 0, colFilled = 0;
        boolean diag1Filled = false, diag2Filled = false;

        // Checks rows for cats game
        for (int r = 0; r < 3; r++) {
            // Tracks how many X's and O's are in the row
            int rowX = 0, rowO = 0;
            for (int c = 0; c < 3; c++) {
                if (board[r][c] == 'X') ++rowX;
                else if (board[r][c] == 'O') ++rowO;
            }

            if (rowX != 0 && rowO != 0) ++rowFilled;
        }

        // Checks columns for cats game
        for (int c = 0; c < 3; c++) {
            // Tracks how many X's and O's are in the column
            int colX = 0, colO = 0;
            for (int r = 0; r < 3; r++) {
                if (board[r][c] == 'X') ++colX;
                else if (board[r][c] == 'O') ++colO;
            }

            if (colX != 0 && colO != 0) ++colFilled;
        }

        int diagX = 0, diagO = 0;
        for (int d = 0; d < 3; d++) {
            if (board[d][d] == 'X') ++diagX;
            else if (board[d][d] == 'O') ++diagO;
        }

        if (diagX != 0 && diagO != 0) diag1Filled = true;

        diagX = 0;
        diagO = 0;
        int dec = 2;
        for (int inc = 0; inc < 3; inc++) {
            if (board[dec][inc] == 'X') ++diagX;
            else if (board[dec][inc] == 'O') ++diagO;
            --dec;
        }

        if (diagX != 0 && diagO != 0) diag2Filled = true;

        // If each row, column, and diagonal have both an X and an O,
        // then it's a tie, return true.
        if (rowFilled == 3 && colFilled == 3 && diag1Filled && diag2Filled)
            return true;

        return false;
    }
}
