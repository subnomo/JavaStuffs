import java.util.Scanner;


public class GuessingGame {

	/**
	 * This is a guessing step
	 */
	
	// Creates the scanner for getting input
	private static Scanner readData = new Scanner(System.in);
	
	public static void main(String[] args) {
		new GuessingGame();
	}
	
	public GuessingGame() {
		boolean cont = true;
		while (cont) {
			cont = play();
		}
		System.out.println("Goodbye...");
	}

	public static boolean play() {
		int guesses = 0;
		int randNum = (int)(Math.random() * (1001));
		String randString = String.valueOf(randNum);
		char[] randLetters = randString.toCharArray();
		while (true) {
			guesses++;
			System.out.println("Guess a number: ");
			int guess = readData.nextInt();
			
			int correct = 0;
			String guessString = String.valueOf(guess);
			char[] guessLetters = guessString.toCharArray();
			
			if (randLetters.length >= guessLetters.length) {
				for (int i = 0; i < guessLetters.length; i++) {
					if (guessLetters[i] == randLetters[i]) correct++;
				}
			} else {
				for (int i = 0; i < randLetters.length; i++) {
					if (guessLetters[i] == randLetters[i]) correct++;
				}
			}
			
			if (guess != randNum) {
				System.out.println("You have " + correct + " correct digits.");
			} else {
				System.out.println("You were correct after " + guesses + " tries!");
				System.out.println("Do you want to try again? (y/n): ");
				String repeat = readData.next();
				
				if ("yes".equals(repeat) || "y".equals(repeat)) return true;
				else return false;
			}
		
		}
	}

}
