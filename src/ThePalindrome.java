import java.util.Scanner;

public class ThePalindrome {

	/**
	 * Palindrome generator
	 */

	// Creates the scanner for getting input
	private static Scanner readData = new Scanner(System.in);

	public static void main(String[] args) {
		new ThePalindrome();
	}

	public ThePalindrome() {
		System.out.println("Enter text to generate a palindrome: ");
		String text = readData.next();
		find(text.toLowerCase());
	}

	public static void find(String s) {
		if (s.length() > 50) {
			System.out.println("The string cannot be longer than 50 characters.");
			return;
		}
		
		if (s.length() == 1) {
			System.out.println("1");
			return;
		} else if (s.length() == 2) {
			System.out.println("4");
			return;
		}
		
		String reverse = new StringBuilder(s).reverse().toString();
		if (s.equals(reverse)) {
			System.out.println(s.length()); // Already a palindrome
		} else {
			// Do the palindrome things
			char[] str = s.toCharArray();
			System.out.println(str.length);
			
			for (int i = 0; i < str.length; i++)
				for (int j = i + 1; j < str.length; j++)
					if (i != j && str[i] == str[j]) {
						// Then duplicates
					}
		}

	}

}
