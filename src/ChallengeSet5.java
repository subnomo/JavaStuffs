import java.util.Scanner;


public class ChallengeSet5 {

	// Creates the scanner for getting input
	private static Scanner readData = new Scanner(System.in);
	
	public static void main(String[] args) {
		new ChallengeSet5();
	}
	
	public ChallengeSet5() {
		System.out.println("Which problem number do you wish to run?");
		System.out.println("\n1. Grades Revisited" +
				"\n2. Roll the Dice" +
				"\n3. Queen's Choice" +
				"\n4. Histogram" +
				"\n5. Making a List... Checking it Twice" +
				"\n6. A Unique List");
		
		int choice = readData.nextInt();
		switch (choice) {
		case 1:
			grades();
			break;
		case 2:
			dice();
			break;
		case 3:
			queens();
			break;
		case 4:
			histogram();
			break;
		case 5:
			checkList();
			break;
		case 6:
			uniqueList();
			break;
		default:
			System.out.println("\nUnexpected input. Closing...");
			break;
		}
	}
	
	public static void grades() {
		System.out.println("Number of grades to enter: ");
		int numGrades = readData.nextInt();
		double grade[] = new double[numGrades];
		double total = 0;
		
		for (int i = 0; i < numGrades; i++) {
			System.out.println("Enter the grade: ");
			grade[i] = readData.nextDouble();
			total += grade[i];
		}
		
		double mean = total / numGrades;
		int numFailures = 0;
		
		// Not sure if there is a better way...
		for (int i = 0; i < numGrades; i++) if (grade[i] < mean) numFailures++;
		
		double failures[] = new double[numFailures];
		int nextIndex = -1;
		for (int i = 0; i < numGrades; i++) {
			if (grade[i] < mean) {
				failures[++nextIndex] = grade[i];
			}
		}
		System.out.print("These grades are below the mean: ");
		for (int i = 0; i < numFailures; i++) {
			System.out.print(failures[i]);
			if (numFailures != 1 && i != numFailures - 1) System.out.print(", ");
		}
		
		
	}
	
	public static void dice() {
		System.out.print("How many times would you like to roll the die? ");
		int rolls = readData.nextInt();
		int rands[] = new int[rolls];
		
		for (int i = 0; i < rolls; i++) rands[i] = (int)(Math.random() * 6) + 1;
		int one = 0, two = 0, three = 0, four = 0, five = 0, six = 0;
		for (int i = 0; i < rolls; i++) {
			switch(rands[i]) {
			case 1:
				++one;
				break;
			case 2:
				++two;
				break;
			case 3:
				++three;
				break;
			case 4:
				++four;
				break;
			case 5:
				++five;
				break;
			case 6:
				++six;
				break;
			default:
				System.out.println("This shouldn't happen. Whoops!");
				break;
			}
		}
		System.out.println("1: " + one + "\n2: " + two + "\n3: " + three + "\n4: "
		+ four + "\n5: " + five + "\n6: " + six);
		
	}
	
	public static void queens() {
		int queenColumn[] = new int[8];
		System.out.println("Which columns do you want the queen to be in?");
		for (int i = 0; i < 8; i++) {
			System.out.print("Row " + (i + 1) + ": ");
			queenColumn[i] = readData.nextInt();
		}
		
		System.out.println();
		int pos = 0;
		// For each row
		for (int r = 0; r < 8; r++) {
			boolean q = false;
			
			// For each column in that row
			for (int c = 0; c < 8; c++) {
				// If a queen hasn't already been printed for that row
				// AND
				if (q == false && queenColumn[pos] == c + 1) {
					System.out.print("Q");
					q = true;
					++pos;
				} else System.out.print("*");
			}
			System.out.println();
		}
	}
	
	public static void histogram() {
		int counter = 0; // Useful for later, counts total iterations
		
		int inBigRange[] = new int[9]; // An array to track the grades in all ranges 10 - 99
		System.out.println("Number of grades to enter: ");
		int numGrades = readData.nextInt();
		int grades[] = new int[numGrades];
		for (int i = 0; i < numGrades; i++) {
			if (i == 0) System.out.print("1st: ");
			else if (i == 1) System.out.print("2nd: ");
			else if (i == 2) System.out.print("3rd: ");
			else System.out.print((i + 1) + "th: ");
			
			grades[i] = readData.nextInt();
		}
		
		System.out.println();
		
		for (int i = 0; i <= 100; i++) { // Loops 1-100
			char[] num = String.valueOf(i).toCharArray(); // Converts i to a char array
			
			// this is for ranges 0 - 9
			if (num.length == 1) { // Checks length of char array (checks # of characters of i)
				int inrange = 0; // Integer to keep track of grades in that range
				
				for (int grade = 0; grade < grades.length; grade++) {
					if (grades[grade] >= 0 && grades[grade] <= 9)
						++inrange;
				}
				
				if (i == 0)
					System.out.print("  " + i + "-");
				else if (i == 9) {
					System.out.print(i + ":");
					
					if (inrange != 0) {
						for (int star = 1; star <= inrange; star++) {
							System.out.print("*");
							
							// Prints newline when finished printing stars
							if (star == inrange)
								System.out.println();
						}
					} else System.out.println();
				}
			} else if (num.length == 2) { // this is for ranges 10 - 99 (two digit)
				int tens = (int)num[0] - '0'; // Converts tens place of num from char to int
				int lowest = Integer.parseInt(num[0] + "0");
				int highest = Integer.parseInt(num[0] + "9");
				
				for (int grade = 0; grade < grades.length; grade++) {
					if (grades[grade] >= lowest && grades[grade] <= highest) {
						if (counter == 9) { // Only executes 1/10 of the time (usually 900 iterations)
							++inBigRange[tens - 1];
							counter = 0;
						}
						++counter;
					}
						
				}
				
				if (num[1] == 48) // If the second character is zero
					System.out.print(i + "-");
				else if (num[1] == 57) { // If the second character is nine
					System.out.print(i + ":");
					
					// Loops through all the ranges of inBigRange[] array
					if (inBigRange[tens - 1] != 0) {
						for (int star = 1; star <= inBigRange[tens - 1]; star++) {
							System.out.print("*");
							
							// Prints newline when finished printing stars
							if (star == inBigRange[tens - 1])
								System.out.println();
						}
					} else System.out.println();
				}
				
			} else { // this is specifically for 100
				int inrange = 0;
				
				for (int grade = 0; grade < grades.length; grade++) {
					if (grades[grade] == 100)
						++inrange;
				}
				
				System.out.print("  100:");
				
				if (inrange != 0)
					for (int star = 1; star <= inrange; star++) System.out.print("*");
			}
		}
	}
	
	public static void checkList() {
		// Generate 20 random numbers from 1-20
		int rands[] = new int[20];
		for (int i = 0; i < 20; i++) rands[i] = (int)(Math.random() * 20) + 1;
		
		System.out.println("Full set of random numbers:");
		for (int i = 0; i < 20; i++)
			System.out.print(rands[i] + " ");
		
		System.out.println("\nNumbers without repetition:");
		boolean checked = false;
		// For each number
		for (int i = 0; i < 20; i++) {
			checked = false;
			// If number has already been printed, don't print
			for (int r = 0; r < i; r++)
				if (rands[i] == rands[r]) checked = true;
			// Otherwise, print it out
			if (checked == false)
				System.out.print(rands[i] + " ");
		}
	}
	
	// Small method for checking for duplicates in array
	public static boolean checkArray(int arr[]) {
		for (int i = 0; i < arr.length; i++)
			for (int r = 0; r < i; r++)
				if (arr[i] == arr[r]) return true; // There are repeats
		return false; // No repeats
	}
	
	// Not the most efficient memory-wise because of recursion...
	public static void uniqueList() {
		int rands[] = new int[20];
		for (int i = 0; i < 20; i++) rands[i] = (int)(Math.random() * 100) + 1;
		
		// If there are duplicates, executes the function again and again until there are none.
		if (checkArray(rands)) uniqueList();
		else for (int i = 0; i < 20; i++) System.out.print(rands[i] + " ");
	}
}