//import java.util.ArrayList;
import java.util.Scanner;


public class ChallengeSet3 {

	/**
	 * The third challenge set
	 */
	
	// Creates the scanner for getting input
	private static Scanner readData = new Scanner(System.in);
	
	public static void main(String[] args) {
		new ChallengeSet3();
	}
	
	public ChallengeSet3() {
		System.out.println(int2rat(2003));
		addUp();
		grades();
		multChoice();
		reciprocal();
		//test();
	}
	
	public static void addUp() {
		System.out.println("Enter an integer: ");
		int value = readData.nextInt();
		
		// For loop would be easier
		int x = 1, sum = 0;
		while (x <= value) {
			sum += x;
			x++;
		}
		System.out.println(sum);
	}
	
	public static void grades() {
		int x = 0, // Keeps count
		a = 0, // Grades
		b = 0,
		c = 0,
		d = 0,
		f = 0;
		double gradeSum = 0.0;
		while (true) {
			System.out.println("Enter test grade: ");
			double grade = readData.nextDouble();
			
			if (grade == -1) break;
			
			if (grade >= 90) a++;
			else if (grade >= 80) b++;
			else if (grade >= 70) c++;
			else if (grade >= 60) d++;
			else f++;
			
			x++;
			gradeSum += grade;
		}
		double avgGrade = gradeSum / x;
		System.out.println("The average grade is: " + avgGrade);
		System.out.println("A: " + a + " B: " + b + " C: " + c + " D: " + d + " F: " + f);
	}
	
	public static void multChoice() {
		System.out.println("What is the name of the command that allows a method to send a value to another method?");
		
		while (true) {
			System.out.println("1. Parameter");
			System.out.println("2. Constructor");
			System.out.println("3. Return");
			System.out.println("4. George");
			
			int ans = readData.nextInt();
			if (ans == 3) {
				System.out.println("Nice job!");
				break;
			} else {
				System.out.println("Sorry, that's incorrect. Try again? (y/n): ");
				String tryAgain = readData.next();
				
				if ("yes".equals(tryAgain) || "y".equals(tryAgain));
				else break;
			}
		}
	}
	
	
	// This method is magic
	public static int gcm(int a, int b) {
		System.out.println("A: " + a + "\tB: " + b);
		return b == 0 ? a : gcm(b, a % b); // If b = 0 then return a, else calls itself
	}
	
	public static String asFraction(int a, int b) {
	    int gcm = gcm(a, b);
	    if ((b / gcm) == 1) return String.valueOf((a / gcm));
	    return (a / gcm) + "/" + (b / gcm);
	}
	
	public static String int2rat(int a) {
		String aString = Double.toString(a);        
		String[] fraction = aString.split("\\.");

		int denominator = (int)Math.pow(10, fraction[1].length());
		int numerator = Integer.parseInt(fraction[0] + "" + fraction[1]);
		return asFraction(numerator, denominator);
	}

	public static void reciprocal() {
		/*boolean err = false;
		ArrayList<Integer> sum = new ArrayList<Integer>();*/
		int sum = 0;
		int x = 0;
		while (x < 10) {
			System.out.println("Enter a number: ");
			int number = readData.nextInt();
			
			if (number == 0) {
				System.out.println("Stop adding numbers? (y/n): ");
				String stop = readData.next();
				
				if ("yes".equals(stop) || "y".equals(stop)) break;
			}
			x++;
			sum += number;
		}
		
		
		/*String recip = "";
		int mult = 0;
		switch (sum.size()) { // This REALLY sucks, but this will do for now
		case 1:				  // because I need to move on. I will fix it sometime.
			recip = "1 / " + sum.get(0);
			break;
		case 2:
			mult = gcm(sum.get(0), sum.get(1));
			break;
		case 3:
			mult = gcm(sum.get(0), gcm(sum.get(1), sum.get(2)));
			break;
		case 4:
			mult = gcm(sum.get(0), gcm(sum.get(1), gcm(sum.get(2), sum.get(3))));
			break;
		case 5: // Can only handle up to 5 inputed integers
			mult = gcm(sum.get(0), gcm(sum.get(1), gcm(sum.get(2), gcm(sum.get(3), sum.get(4)))));
			break;
		default:
			System.out.println("Error!");
			err = true;
			break;
		}
		if (err) return;
		
		
		System.out.println("The sum of the reciprocals is: " + recip);*/
	}
}
