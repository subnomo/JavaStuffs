import java.util.Scanner;

public class TicTacToe {
    static char player = 'X';
    static char[][] board = new char[3][3];

    // Creates the scanner for getting input
    private static Scanner readData = new Scanner(System.in);

    public static void main(String[] args) {
        new TicTacToe();
    }

    public TicTacToe() {
        ticTacToe();
    }

    public static void ticTacToe() {
        // Run until there is a win/draw
        do {
            // If an error occurs, keep restarting playerGo()
            while (!playerGo());

            // If it's a cat's step, exit
            if (catsGame()) {
                updateBoard();
                System.out.println("Cats step! Everyone's a loser!");
                return;
            }
        } while (!checkWin());

        // Switches the player on win
        if (player == 'X')
            player = 'O';
        else
            player = 'X';

        updateBoard();
        System.out.println(player + " Wins!");
    }

    public static void updateBoard() {
        // Prints 1 2 3
        System.out.println("\t1\t2\t3");
        for (int r = 0; r < 3; r++) {
            System.out.print(Character.toChars(97 + r));
            System.out.print("\t");
            for (int c = 0; c < 3; c++)
                // If the position is occupied, print
                // Else, print a blank space
                if (board[r][c] == 'X' || board[r][c] == 'O')
                    System.out.print(board[r][c] + "\t");
                else
                    System.out.print(" " + "\t");
            System.out.println();
        }
    }

    // Gets player input
    public static boolean playerGo() {
        updateBoard();

        System.out.println(player + "'s turn. Where would you like to move? (e.g. c2)");
        char[] pos = readData.next().toCharArray();

        // Some error handling
        if (pos.length < 2) {
            System.out.println("You didn't enter a correct position!");
            return false;
        } else if (!((Object) pos[0]).getClass().equals(Character.class)) {
            System.out.println("Unexpected value '" + pos[0] + "'");
            return false;
        } else if (!((Object) pos[1]).getClass().equals(Character.class)) {
            System.out.println("Unexpected value '" + pos[1] + "'");
            return false;
        }

        int row = Character.toLowerCase(pos[0]) - 97;
        int column = pos[1] - 49;

        // Some more error handling
        if (row < 0 || row > 2) {
            System.out.println("Unexpected value '" + pos[0] + "'");
            return false;
        } else if (column < 0 || column > 2) {
            System.out.println("Unexpected value '" + pos[1] + "'");
            return false;
        } else if (board[row][column] == 'X' || board[row][column] == 'O') {
            System.out.println("Illegal move, position occupied.");
            return false;
        }

        board[row][column] = player;

        // Switches the player turn
        if (player == 'X')
            player = 'O';
        else
            player = 'X';

        return true;

    }

    // Checks board for winner
    public static boolean checkWin() {
        // Checks rows for win
        for (int r = 0; r < 3; r++) {
            // Tracks how many X's and O's are in the row
            int rowX = 0, rowO = 0;
            for (int c = 0; c < 3; c++) {
                if (board[r][c] == 'X') ++rowX;
                else if (board[r][c] == 'O') ++rowO;
            }

            if (rowX == 3 || rowO == 3) return true;
        }

        // Checks columns for win
        for (int c = 0; c < 3; c++) {
            // Tracks how many X's and O's are in the column
            int colX = 0, colO = 0;
            for (int r = 0; r < 3; r++) {
                if (board[r][c] == 'X') ++colX;
                else if (board[r][c] == 'O') ++colO;
            }

            if (colX == 3 || colO == 3) return true;
        }

        // Checks diagonals for win
        int diagX = 0, diagO = 0;
        for (int d = 0; d < 3; d++) {
            if (board[d][d] == 'X') ++diagX;
            else if (board[d][d] == 'O') ++diagO;
        }

        if (diagX == 3 || diagO == 3) return true;

        diagX = 0;
        diagO = 0;
        int dec = 2;
        for (int inc = 0; inc < 3; inc++) {
            if (board[dec][inc] == 'X') ++diagX;
            else if (board[dec][inc] == 'O') ++diagO;
            --dec;
        }

        if (diagX == 3 || diagO == 3) return true;

        return false;
    }

    // Checks board for cat's step
    public static boolean catsGame() {
        int rowFilled = 0, colFilled = 0;
        boolean diag1Filled = false, diag2Filled = false;

        // Checks rows for cats step
        for (int r = 0; r < 3; r++) {
            // Tracks how many X's and O's are in the row
            int rowX = 0, rowO = 0;
            for (int c = 0; c < 3; c++) {
                if (board[r][c] == 'X') ++rowX;
                else if (board[r][c] == 'O') ++rowO;
            }

            if (rowX != 0 && rowO != 0) ++rowFilled;
        }

        // Checks columns for cats step
        for (int c = 0; c < 3; c++) {
            // Tracks how many X's and O's are in the column
            int colX = 0, colO = 0;
            for (int r = 0; r < 3; r++) {
                if (board[r][c] == 'X') ++colX;
                else if (board[r][c] == 'O') ++colO;
            }

            if (colX != 0 && colO != 0) ++colFilled;
        }

        int diagX = 0, diagO = 0;
        for (int d = 0; d < 3; d++) {
            if (board[d][d] == 'X') ++diagX;
            else if (board[d][d] == 'O') ++diagO;
        }

        if (diagX != 0 && diagO != 0) diag1Filled = true;

        diagX = 0;
        diagO = 0;
        int dec = 2;
        for (int inc = 0; inc < 3; inc++) {
            if (board[dec][inc] == 'X') ++diagX;
            else if (board[dec][inc] == 'O') ++diagO;
            --dec;
        }

        if (diagX != 0 && diagO != 0) diag2Filled = true;

        // If each row, column, and diagonal have both an X and an O,
        // then it's a tie, return true.
        if (rowFilled == 3 && colFilled == 3 && diag1Filled && diag2Filled)
            return true;

        return false;
    }
}