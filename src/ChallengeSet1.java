import java.util.Scanner;


public class ChallengeSet1 {

	/**
	 * The Challenges
	 */
	
	private static Scanner readData = new Scanner(System.in); // Why is input so hard
	public static void main(String[] args) {
		new ChallengeSet1();
	}
	
	public ChallengeSet1() {
		System.out.println("***PROBLEM #1***\n");
		diamond();
		System.out.println("\n");
		arrow();
		System.out.println("\n");
		square();
		System.out.println("\n");
		landscape();
		System.out.println("\n***PROBLEM #2***\n");
		numbers();
		System.out.println("\n***PROBLEM #3***\n");
		circle();
		System.out.println("\n***PROBLEM #4***\n");
		mpg();
	}
	
	// This prints a diamond
	public static void diamond() {
		System.out.println("/\\\n\\/");
	}
	
	// This prints an arrow
	public static void arrow() {
		System.out.println("/|\\\n |\n |\n |\n |\n");
	}
	
	// This prints a square
	public static void square() {
		System.out.println("''''''''\n'      '\n'      '\n''''''''");
	}
	
	// This prints a tree
	public static void landscape() {
		System.out.println("        .-'- -.\n       (       )\n      (         )\n       ( \\'./^ )\n        '-| |-'\n          | |\n      \"\"\"\"\"\"\"\"\"\"\"\"");
	}
	
	// This adds, subtracts, multiplies, and divides two inputed numbers
	public static void numbers() {
		System.out.println("Enter the first number: ");
		int num1 = readData.nextInt();
		System.out.println("Enter the second number: ");
		int num2 = readData.nextInt();
		
		System.out.println(num1 + " + " + num2 + " = " + (num1 + num2) + "\n" + num1 + " - " + num2 + " = " + (num1 - num2) + "\n" + num1 + " * " + num2 + " = " + (num1 * num2) + "\n" + num1 + " / " + num2 + " = " + (num1 / num2));
	}
	
	// This takes an inputed radius and prints out different 
	public static void circle() {
		System.out.println("Enter the radius of the circle: ");
		double r = readData.nextDouble();
		double pi = 3.14159;
		double circ = 2 * r;
		double area = pi * r * r;
		double diam = 2 * pi * r;
		
		System.out.println("Circumference: " + circ + "\nArea: " + area + "\nDiameter: " + diam);
	}
	
	// Calculates used fuel, miles traveled, and miles per gallon using inputed values
	// and prints them out
	public static void mpg() {
		System.out.println("Enter the first odometer reading: ");
		int firstOdom = readData.nextInt();
		System.out.println("Enter the last odometer reading: ");
		int lastOdom = readData.nextInt();
		System.out.println("Enter the first fuel reading: ");
		int firstFuel = readData.nextInt();
		System.out.println("Enter the last fuel reading: ");
		int lastFuel = readData.nextInt();
		
		int fuelUsed = firstFuel - lastFuel;
		int milesTraveled = lastOdom - firstOdom;
		int milesPerGallon = milesTraveled / fuelUsed;
		
		System.out.println("Total fuel used: " + fuelUsed + "\nTotal Miles Traveled: " + milesTraveled + "\nMiles Per Gallon: " + milesPerGallon);
	}
}
