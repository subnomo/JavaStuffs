import java.util.Scanner;


public class ChallengingChallengeSet3 {

	/**
	 * Challenge 3 Additional Challenges
	 */
	
	// Creates the scanner for getting input
	private static Scanner readData = new Scanner(System.in);
	
	public static void main(String[] args) {
		new ChallengingChallengeSet3();
	}
	
	public ChallengingChallengeSet3() {
		addUp();
	}
	
	public static void addUp() {
		System.out.println("Enter an integer: ");
		int value = readData.nextInt();
		
		// For loop would be easier
		int x = 1, sum = 0;
		while (x <= value) {
			sum += x;
			x++;
		}
		System.out.println(sum);
	}

}
