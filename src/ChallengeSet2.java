/*
import java.util.Scanner;


public class ChallengeSet2 {

	*/
/**
	 * ChallengeSet2
	 *//*

	
	// Creates the scanner for getting input
	private static Scanner readData = new Scanner(System.in);
	
	// Main method, calls constructor
	public static void main(String[] args) {
		new ChallengeSet2();
	}
	
	// Constructor
	public ChallengeSet2() {
		while (true) {
			System.out.println("\n\nDo you want to run:\n1. EvenOdd check\n2. Age check\n3. BMI\n4. Name rater\n5. Number splitter");
			int choice = readData.nextInt();
			switch (choice) {
				case 1:
					isEven();
					break;
				case 2:
					isOldEnough();
					break;
				case 3:
					BMI();
					break;
				case 4:
					name();
					break;
				case 5:
					split();
					break;
			}
		}
	}
	
	// Gets inputed value and checks whether even or odd
	public static void isEven() {
		System.out.println("Please enter a number: ");
		int num = readData.nextInt();
		
		if ((num % 2) == 0) {
			System.out.println("The number is even.");
		} else {
			System.out.println("The number is odd.");
		}
	}
	
	// Takes the inputed integer from other methods and does the above,
	// this time the method only returns true or false.
	public static boolean newIsEven(int number) {
		if ((number % 2) == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	// Takes inputed integer for age and prints whether or not they are of age
	// also checks if the age is even or odd, prints the result
	public static void isOldEnough() {
		System.out.println("Enter your age: ");
		int age = readData.nextInt();
		
		if (age >= 18) {
			System.out.println("Yes.");
		} else if (age <= 0) {
			System.out.println("Please pick a number larger than 0.");
		} else {
			System.out.println("No.");
		}
		
		if (age >= 16) {
			System.out.println("You can drive.");
		}
		if (age >= 17) {
			System.out.println("You can be a blood donor.");
		}
		if (age >= 21) {
			System.out.println("You can drink alcohol.");
		}
		if (age >= 35) {
			System.out.println("You can be president.");
		}
		
		boolean even = newIsEven(age);
		if (even) {
			System.out.println("This number is even.");
		} else {
			System.out.println("This number is odd.");
		}
	}
	
	*/
/*
	 * I misinterpreted the rules, so I didn't use if statements at all.
	 * Used switch case and ternary operators to calculate and print the
	 * user's BMI. Also checks if the weight/height is even with the newIsEven method.
	 *//*

	public static void BMI() {
		System.out.println("Do you know your weight and height? ");
		String ans = readData.next();
		
		switch (ans) {
		case "yes":
		case "y":
			System.out.println("Enter your weight: ");
			int weight = readData.nextInt();
			
			String evenOdd = newIsEven(weight) == true ? "even" : "odd";
			System.out.println("That number is " + evenOdd);
			
			System.out.println("Enter your height (in inches): ");
			int height = readData.nextInt();
			
			evenOdd = newIsEven(height) == true ? "even" : "odd";
			System.out.println("That number is " + evenOdd);
			
			double BMI = (weight * 703) / (height * height);
			System.out.println("Your BMI is: " + BMI);
			
			// If BMI is less than 25: (if BMI is less than 18.5: String state = 
			// "underweight" else: String state = "normal") else: (if BMI
			// is more than 30: String state = "obese" else: String state = "overweight")
			String state = (BMI < 25) ? (BMI < 18.5 ? "underweight" : "normal") : (BMI > 30 ? "obese" : "overweight");
			System.out.println("You are " + state);
			break;
		case "no":
		case "n":
			System.out.println("Take some measurements, then come back to me.");
			break;
		default:
			System.out.println("You entered something unexpected, closing...");
			break;
		}
	}
	
	// Gets name input and compares to the best name
	public static void name() {
		System.out.println("Enter your name: ");
		String name = readData.next();
		
		if ("Alex".equalsIgnoreCase(name) || "Alexander".equalsIgnoreCase(name)) {
			System.out.println("Wow, you have a really cool name!");
		} else {
			System.out.println("Eh.");
		}
	}
	
	// I cheated and got a string as input, then split it into five characters
	public static void split() {
		System.out.println("Enter a five-digit integer: ");
		String num = readData.next();
		
		char[] numbers = num.toCharArray();
		System.out.println(numbers[0] + " " + numbers[1] + " " + numbers[2] + " " + numbers[3] + " " + numbers[4]);
		
	}

}
*/
