import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Meinsweeper implements ActionListener {
    static JFrame frame = new JFrame("Meinsweeper");
    static Container center = new Container();
    static JButton reset = new JButton("Reset");
    static JButton[][] jButtons = new JButton[20][20];
    static boolean[][] cells = new boolean[20][20];
    static boolean clickedOnce = false;

    public static void main(String[] args) {
        new Meinsweeper();
    }

    public Meinsweeper() {
        // Set up the window
        frame.setSize(900, 900);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // Set up other things

        frame.setLayout(new BorderLayout());
        center.setLayout(new GridLayout(20, 20));
        for (int r = 0; r < jButtons.length; r++) {
            for (int c = 0; c < jButtons.length; c++) {
                // This line is necessary for inner class MouseAdapter
                final int x = r, y = c;

                jButtons[r][c] = new JButton();
                jButtons[r][c].setBackground(Color.WHITE);
                center.add(jButtons[r][c]);
                jButtons[r][c].addActionListener(this);
                jButtons[r][c].addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (e.getButton() == MouseEvent.BUTTON3 && clickedOnce) {
                            toggleFlag(x, y);
                            checkWin();
                        }
                    }
                });
            }
        }

        reset.setFocusPainted(false);
        reset.addActionListener(this);

        frame.add(reset, BorderLayout.NORTH);
        frame.add(center, BorderLayout.CENTER);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(reset))
            reset();
        else {
            for (int r = 0; r < jButtons.length; r++) {
                for (int c = 0; c < jButtons.length; c++) {
                    if (e.getSource().equals(jButtons[r][c])) {
                        jButtons[r][c].setFocusPainted(false);

                        if (!clickedOnce) {
                            placeMines(r, c);
                            clickedOnce = true;
                        }

                        // If the clicked cell contains a bomb, player loses
                        if (cells[r][c])
                            lose();
                        else {
                            reveal(r, c, false);
                            checkWin();
                        }
                    }
                }
            }
        }
    }

    public static void placeMines(int x, int y) {
        // Generate 60 random mines
        for (int i = 0; i < 60; i++) {
            int r = randInt(0, 19), c = randInt(0, 19);

            // If there is a mine there already or if the cell is the starting cell, generate random coordinate again.
            while (cells[r][c] || (x == r && y == c)) {
                r = randInt(0, 19);
                c = randInt(0, 19);
            }

            cells[r][c] = true;
        }
    }

    public static void reveal(int r, int c, boolean simple) {
        int nonBombs = getNonBombNeighbors(getNeighbors(r, c)).size();
        int surrounding = 8;

        int border = onBorder(r, c);
        if (border != 0) {
            if (border == 1)
                surrounding = 5;
            else if (border == 2)
                surrounding = 3;
        }

        int bombsAround = surrounding - nonBombs;
        if (bombsAround != 0)
            jButtons[r][c].setText(String.valueOf(bombsAround));

        jButtons[r][c].setBackground(Color.lightGray);
        jButtons[r][c].setEnabled(false);

        // If simple is true, do not do recursion things
        if (simple)
            return;

        // If the cell is a zero cell, recursion stuff
        if (nonBombs == 8) {
            int[][] neighbors = getNeighbors(r, c);
            for (int neighbor = 0; neighbor < 8; neighbor++) {
                int  x = neighbors[neighbor][0], y = neighbors[neighbor][1];

                if (x >= 0 && y >= 0 && x <= 19 && y <= 19) {
                    if (getNonBombNeighbors(getNeighbors(x, y)).size() == 8 && jButtons[x][y].getBackground().equals(Color.WHITE))
                        reveal(x, y, false);
                    else if (getNonBombNeighbors(getNeighbors(x, y)).size() != 8 && jButtons[x][y].getBackground().equals(Color.WHITE))
                        reveal(x, y, true);
                }
            }
        }
    }

    public static void lose() {
        for (int r = 0; r < cells.length; r++) {
            for (int c = 0; c < cells.length; c++) {
                if (cells[r][c]) {
                    jButtons[r][c].setBackground(Color.BLACK);
                } else {
                        if (jButtons[r][c].getBackground().equals(Color.WHITE) || jButtons[r][c].getBackground().equals(Color.RED))
                            jButtons[r][c].setBackground(Color.WHITE);
                        else
                            jButtons[r][c].setBackground(Color.lightGray);
                }
                jButtons[r][c].setEnabled(false);
            }
        }
    }

    public static void checkWin() {
        for (int r = 0; r < jButtons.length; r++) {
            for (int c = 0; c < jButtons.length; c++) {
                // If there is a bomb and it's not flagged, no win
                if (cells[r][c] && !jButtons[r][c].getBackground().equals(Color.RED))
                    return;
                // If there is no bomb there but a flag, no win
                else if (!cells[r][c] && jButtons[r][c].getBackground().equals(Color.RED))
                    return;
            }
        }

        JOptionPane.showMessageDialog(frame, "Congratulations! You win!");
        reset();
    }

    public static void reset() {
        clickedOnce = false;

        for (int r = 0; r < jButtons.length; r++) {
            for (int c = 0; c < jButtons.length; c++) {
                JButton current = jButtons[r][c];

                current.setBackground(Color.WHITE);
                current.setText("");
                current.setEnabled(true);

                cells[r][c] = false;
            }
        }
    }

    public static void toggleFlag(int r, int c) {
        if (jButtons[r][c].getBackground().equals(Color.lightGray))
            return;

        if (jButtons[r][c].getBackground().equals(Color.WHITE))
            jButtons[r][c].setBackground(Color.RED);
        else jButtons[r][c].setBackground(Color.WHITE);
    }

    public static int[][] getNeighbors(int x, int y) {
        int[] pos1 = new int[2];
        int[] pos2 = new int[2];
        int[] pos3 = new int[2];
        int[] pos4 = new int[2];
        int[] pos5 = new int[2];
        int[] pos6 = new int[2];
        int[] pos7 = new int[2];
        int[] pos8 = new int[2];

        // Starts with upper left corner and goes around clockwise
        pos1[0] = x - 1;
        pos1[1] = y - 1;
        pos2[0] = x;
        pos2[1] = y - 1;
        pos3[0] = x + 1;
        pos3[1] = y - 1;
        pos4[0] = x + 1;
        pos4[1] = y;
        pos5[0] = x + 1;
        pos5[1] = y + 1;
        pos6[0] = x;
        pos6[1] = y + 1;
        pos7[0] = x - 1;
        pos7[1] = y + 1;
        pos8[0] = x - 1;
        pos8[1] = y;

        return new int[][]{pos1, pos2, pos3, pos4, pos5, pos6, pos7, pos8};
    }

    public static List getNonBombNeighbors(int[][] neighbors) {
        List<int[]> nonBombNeighbors = new ArrayList<int[]>();

        // For each neighbor in the list of neighbors
        for (int neighbor = 0; neighbor < 8; neighbor++) {
            int  x = neighbors[neighbor][0], y = neighbors[neighbor][1];
            int[] coordinates = {x, y};

            if (x < 0 || y < 0 || x > 19 || y > 19) {
                // Do nothing
            } else if (!cells[x][y]) {
                //System.out.println("(" + coordinates[0] + ", " + coordinates[1] + ")");
                nonBombNeighbors.add(coordinates);
            }
        }

        return nonBombNeighbors;
    }

    /*
     * This checks to see if the selected cell is on the border. If it on the border but not in a corner,
     * it returns 1. If it is in a corner, it returns 2. If it is not on the border, it returns 0.
     */
    public static int onBorder(int r, int c) {
        if (r == 0 || r == 19) {
            if (c == 0 || c == 19)
                return 2;
            else return 1;
        } else if (c == 0 || c == 19)
            return 1;
        else return 0;
    }

    // Returns a random integer between the minimum and maximum values
    public static int randInt(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }
}
